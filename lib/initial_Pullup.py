import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        layout = QVBoxLayout()
        bottom_box = QHBoxLayout()
        grid_edit = QGridLayout()
        
        label_title = QLabel()
        label_title.setText('<center><font size="7">Input module property</font></center>')

        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_result)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)

        bottom_box.addWidget(Back_button)
        bottom_box.addStretch()
        bottom_box.addWidget(Next_button)

        label_comment = QLabel()
        label_comment.setText('comment : ')
        
        self.edit_comment = QTextEdit()
        self.edit_comment.setText(self.parent.result_info['comment'])

        grid_edit.addWidget(label_comment,1,0)

        outer = QScrollArea()
        outer.setWidgetResizable(True)
        outer.setWidget(self.edit_comment)
        grid_edit.addWidget(outer,1,1)

        layout.addWidget(label_title)
        layout.addLayout( self.make_tab_layout() )
        layout.addStretch()
        layout.addLayout(grid_edit)
        layout.addLayout(bottom_box)
        
        self.setLayout(layout)

    def make_tab_layout(self):

        layout = QHBoxLayout()

        self.TabWidget  = QTabWidget()
        self.radiobutton_group_list_list=[]
        ###############################################################
        ########             image file path                   ########
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/Quad_RD53A.jpg'
            else:
                image_file =''
        elif 'TRIPLET' in self.parent.result_info['ComponentTypeCode'] and 'RING' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/TRIPLET_RING_RD53A.jpg'
            else:
                image_file =''
        elif 'TRIPLET' in self.parent.result_info['ComponentTypeCode'] and 'STAVE' in self.parent.result_info['ComponentTypeCode']:
            if self.parent.result_info['FE_version'] in ['RD53A','practice','No FE chip']:
                image_file = '../image/TRIPLET_STAVE_RD53A.jpg'
            else:
                image_file =''
        else:
            image_file =''
        ###############################################################

        self.add_tab(self.make_standard_layout(image_file),self.TabWidget,'Bullet layout',self.radiobutton_group_list_list)
        if os.path.exists( os.path.join(os.path.dirname(__file__),image_file) ):
            try:
                self.add_tab(self.make_geometrical_layout(image_file),self.TabWidget,'Geometrical layout',self.radiobutton_group_list_list)
            except:
                pass
        else:
            pass
        layout.addWidget(self.TabWidget)
        return layout

    def add_tab(self,layout_tuple,TabWidget,tab_label,list_of_list):
        inner_layout = layout_tuple[0]
        radio_group_list = layout_tuple[1]

        tab = QWidget()
        tab.setLayout( inner_layout )

        list_of_list.append(radio_group_list)
        TabWidget.addTab(tab,tab_label)

    def make_standard_layout(self,image_file):
        layout= QHBoxLayout()
        radio_layout,radio_group_list = self.make_bullet()
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),400,300 )
        if 'TRIPLET' in self.parent.result_info['ComponentTypeCode']:
            image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),600,450 )
        layout.addStretch()
        layout.addLayout(radio_layout)
        layout.addStretch()
        layout.addLayout(image_layout)
        layout.addStretch()

        return (layout, radio_group_list)

    def make_geometrical_layout(self,image_file):
        if 'QUAD' in self.parent.result_info['ComponentTypeCode'] or 'practice' in self.parent.result_info['ComponentTypeCode']:
            layout ,radio_group_list= self.make_quad(image_file)
        if 'TRIPLET' in self.parent.result_info['ComponentTypeCode']:
            layout ,radio_group_list= self.make_triplet(image_file)
        else:
#            QMessageBox.warning(None, 'Warning',self.parent.result_info['type'] +' is not supported for geometrical layout.', QMessageBox.Ok)
            pass
           
        return (layout, radio_group_list)

    def make_image_layout(self,image_path,width,height):
        layout = QHBoxLayout()

        label = QLabel()
        pixmap = QPixmap(image_path)
        scaled_pixmap = pixmap.scaled(width,height,Qt.KeepAspectRatio,Qt.SmoothTransformation)
        label.setPixmap(scaled_pixmap)
        layout.addWidget(label)

        return layout

    def make_bullet(self):

        layout = QVBoxLayout()

        radio_group_list = []
        
        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'
            group_layout,button_group = self.make_group(title_text)
            
            radio_group_list.append(button_group)
            layout.addLayout(group_layout)
        
        return layout, radio_group_list 

    def make_quad(self,image_file):

        layout = QHBoxLayout()
        left_layout = QVBoxLayout()
        right_layout = QVBoxLayout()

        radio_group_list = []
        chip_layout_list = []
        
        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'
            group_layout,button_group = self.make_group(title_text)
            
            radio_group_list.append(button_group)
            chip_layout_list.append(group_layout)

        image_layout = self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),400,300 )
        
        left_layout.addLayout(chip_layout_list[0])
        left_layout.addStretch()
        left_layout.addLayout(chip_layout_list[1])

        right_layout.addLayout(chip_layout_list[3])
        right_layout.addStretch()
        right_layout.addLayout(chip_layout_list[2])

        layout.addStretch()
        layout.addLayout(left_layout)
        layout.addLayout(image_layout)
        layout.addLayout(right_layout)
        layout.addStretch()
       
        return layout, radio_group_list 

    def make_triplet(self,image_file):

        layout = QVBoxLayout()
        up_layout = QHBoxLayout()
        image_layout = QHBoxLayout()

        radio_group_list = []
        chip_layout_list = []
        
        for i in range( self.parent.result_info['N_chip'] ):
            title_text='chip' + str(i+1) + ':'
            group_layout,button_group = self.make_group(title_text)
            
            radio_group_list.append(button_group)
            chip_layout_list.append(group_layout)

        image_layout.addStretch()
        image_layout.addLayout( self.make_image_layout( os.path.join(os.path.dirname(__file__),image_file),720,540 ) )
        image_layout.addStretch()

        up_layout.addLayout(chip_layout_list[0])
        up_layout.addStretch()
        up_layout.addLayout(chip_layout_list[1])
        up_layout.addStretch()
        up_layout.addLayout(chip_layout_list[2])

        layout.addStretch()
        layout.addLayout(up_layout)
        layout.addLayout(image_layout)
       
        return layout, radio_group_list 

    def make_group(self,title_text):
        layout = QHBoxLayout()
        grid_layout= QGridLayout()
        
        radio_group = QButtonGroup()
        radiobutton_list = []
        
        title = QLabel()
        title.setText( title_text )
        layout.addWidget(title)
        
        for j,value in enumerate( self.parent.result_info['choices_resistor'] ):
            
            if self.parent.result_info['unit'] == 'kiloohm':
                label =  str(value) + ' kΩ'
            elif self.parent.result_info['unit'] == 'ohm':
                label =  str(value) + ' Ω' 
            if value ==None:
                label = 'None'

            try:
                if self.parent.result_info['value']['chip'+str(i)] == value:
                    radiobutton_list.append( self.make_radiobutton(label,True) )
                else:
                    radiobutton_list.append( self.make_radiobutton(label,False) )
            except:
                if j==0:
                    radiobutton_list.append( self.make_radiobutton(label,True) )
                else:
                    radiobutton_list.append( self.make_radiobutton(label,False) )

            radio_group.addButton(radiobutton_list[j],j)
            #            layout.addWidget(radiobutton_list[j])

        for i in range( len(radiobutton_list) ):
            grid_layout.addWidget( radiobutton_list[i], i//2, i%2 ) 

        layout.addLayout(grid_layout)
        return layout, radio_group

    def make_radiobutton(self,label,checked):
        radiobutton = QRadioButton(label)
        radiobutton.setCheckable(True)
        radiobutton.setFocusPolicy(Qt.NoFocus)
        radiobutton.setStyleSheet('QRadioButton{font: 15pt;} QRadioButton::indicator { width: 15px; height: 15px;};')        
        try:
            if checked:
                radiobutton.setChecked(True)
        except:
            pass
        return radiobutton

    def pass_result(self):
        try:
            result_dict = {}
            for i in range( self.parent.result_info['N_chip'] ):
                chipID = 'chip' + str(i+1)
                result_dict[chipID] = self.parent.result_info['choices_resistor'][self.radiobutton_group_list_list[ self.TabWidget.currentIndex() ][i].checkedId()] 

            free_comment = self.edit_comment.toPlainText()
        except:
            import traceback
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Warning','Please check the buttons.', QMessageBox.Ok)

        self.parent.recieve_result( result_dict , free_comment )
        
    def back_page(self):
        self.parent.close_and_return()
