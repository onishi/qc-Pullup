import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
from datetime import date, datetime
import traceback


class NotSupportedError(Exception):
    pass

class ConfirmWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent
        
        titlebox= QVBoxLayout()
        layout = QVBoxLayout()
        button_box = QHBoxLayout()

        label_title = QLabel()
        label_title.setText('<center><font size="5">Confirm before uploading to the database</font></center>')
        label_practice = QLabel()
        label_practice.setText('<center><font size="4" color = "green"> Practice Mode</font></center>')
        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        json_button= QPushButton("&Check json (for expert)")
        json_button.clicked.connect(self.check_json)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)
        
        titlebox.addWidget(label_title)
        if self.parent.isPractice:
            titlebox.addWidget(label_practice)

        button_box.addWidget(back_button)
        button_box.addStretch()
        button_box.addWidget(json_button)
        button_box.addWidget(Upload_button)

        inner = QScrollArea()
#        inner.resize(510,425)
#        inner.setFixedHeight(240)
        inner.setFixedHeight(360)
        inner.setFixedWidth(425)
        result_wid = QWidget()
        result_wid.setLayout( self.make_layout() )

        inner.setWidgetResizable(True)
        inner.setWidget(result_wid)

        layout.addLayout(titlebox)
        layout.addWidget(inner)
        layout.addLayout(button_box)
        self.setLayout(layout)
        
    def back_page(self):
        self.parent.back_to_test()

    def check_json(self):
        self.parent.confirm_json()

    def upload_to_db(self):
        self.parent.upload_to_db()

    def add_info(self,Form_layout,label_str,form_text):
        
        label = QLabel()
        label.setText(label_str)

        if label_str =='Comment :':
            inner = QTextEdit()
            inner.setText(form_text)
            inner.setReadOnly(True)
            inner.setFocusPolicy(Qt.NoFocus)
            inner.setStyleSheet("background-color : linen")
            
            editor = QScrollArea()
            editor.setWidgetResizable(True)
            editor.setWidget(inner)

        else:
            editor = QLineEdit()
            editor.setText(form_text)
            editor.setReadOnly(True)
            editor.setFocusPolicy(Qt.NoFocus)
            editor.setStyleSheet("background-color : linen")
        #        editor.setStyleSheet("background-color : azure")

        Form_layout.addRow(label,editor)

#################################################################        
    def make_layout(self):
        
        if self.parent.info_dict['componentType'] == "MODULE":
            return self.layout_ModuleQC()
        elif self.parent.info_dict['componentType'] == 'practice':
            return self.layout_ModuleQC()
        else:
            return self.layout_ModuleQC()
            #            raise NotSupportedError('ComponentType "' + self.parent.info_dict['componentType'] +'" is not supported')

    def layout_ModuleQC(self):

        Form_layout = QFormLayout()
        self.add_info(Form_layout,'Current Stage :',self.parent.result_dict['localDB']['currentStage'])
        self.add_info(Form_layout,'Test Type :',self.parent.result_dict['localDB']['testType'])
        
        
        for i in range( self.parent.info_dict['chip_quantity'] ):
            if self.parent.result_dict['localDB']['results']['unit'] == 'kiloohm':
                unit = 'kΩ'
            elif self.parent.result_dict['localDB']['results']['unit'] == 'ohm':
                unit = 'Ω'

            if self.parent.result_dict['localDB']['results']['value']['chip'+str(i+1)] == None:
                result_value = '{}'.format( self.parent.result_dict['localDB']['results']['value']['chip'+str(i+1)] )
            else:
                result_value = '{} {}'.format( self.parent.result_dict['localDB']['results']['value']['chip'+str(i+1)] , unit )

            self.add_info(Form_layout,'Chip'+str(i+1)+ ' :',result_value)
        self.add_info(Form_layout,'Comment :',self.parent.result_dict['localDB']['results']['comment'])
                
        return Form_layout
        
